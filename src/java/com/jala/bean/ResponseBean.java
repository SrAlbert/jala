package com.jala.bean;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author antonio
 */
public class ResponseBean {
    
    private int errorCode;
    private String msg;
    private Object obj;
    private Object token;
    private String district;
    private String calle;
    private String title;
    private String latitud;
    private String longitud;
    

    public ResponseBean() {
    }

    public ResponseBean(int errorCode) {
        this.errorCode = errorCode;
    }

    public Object getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
    

    public ResponseBean(int errorCode, Object token) {
        this.errorCode = errorCode;
        this.token = (String) token;
    }

    public ResponseBean(int errorCode, String msg) {
        this.errorCode = errorCode;
        this.msg = msg;
    }
    
    public ResponseBean(int errorCode, String msg, Object token) {
        this.errorCode = errorCode;
        this.msg = msg;
        this.token = token;
    }

    public ResponseBean(int errorCode, String msg, String calle, String title, String latitud, String longitud) {
        this.errorCode = errorCode;
        this.msg = msg;
        this.calle = calle;
        this.title = title;
        this.latitud = latitud;
        this.longitud = longitud;
    }
    
    

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setToken(Object token) {
        this.token = token;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }
    
    
    
}
