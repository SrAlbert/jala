package com.jala.dao;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jala.bbdd.Queries;
import com.jala.pojos.Service;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Alberto
 */
public class ServiceDAO extends GenericDAO {

    public ServiceDAO(Session s, Transaction tx) {
        super(s, tx);
    }

    public void add(String name, String level) throws HibernateException, Exception {
        Service s = new Service(name, level);
        getS().persist(s);
    }

    public String biblioteca(String distrito) throws HibernateException, Exception {
                  
                return distrito;
    }

    public String callRestWSByGet(String url) throws IOException {
        URL urlws = new URL(url);
        URLConnection uc = urlws.openConnection();
        uc.connect();
        //Creamos el objeto con el que vamos a leer
        BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
        String inputLine;
        String contenido = "";
        while ((inputLine = in.readLine()) != null) {
            contenido += inputLine + "\n";
        }
        in.close();
        return contenido;
    }

}
