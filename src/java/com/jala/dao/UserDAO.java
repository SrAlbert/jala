package com.jala.dao;

import com.jala.bbdd.Queries;
import com.jala.pojos.Service;
import com.jala.pojos.Token;
import com.jala.pojos.User;
import com.jala.pojos.UserHasService;
import java.util.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Alberto
 */
public class UserDAO extends GenericDAO {

    private final int TIEMPO_TOKEN_ACTIVO = 10;

    public UserDAO(Session s, Transaction tx) {
        super(s, tx);
    }

    /**
     * Metodo que nos permite dar de alta a nuevos usuarios indicandole su
     * nombre de usuario, contraseña y el nivel de acceso a los servicios. Es
     * tipo PUT.
     *
     * @param username String de usuarios.
     * @param password String de contraseña.
     * @param level String del nivel.
     * @return true cuando el usuario se ha creado correctamente
     * @throws HibernateException
     * @throws Exception
     */
    public boolean register(String username, String password, String level) throws HibernateException, Exception {
        Query q = getS().createQuery(Queries.CHECK_USER);
        q.setParameter("username", username);
        User u = (User) q.uniqueResult();
        if (u == null) {
            User user = new User(username, encrypt(password), level);
            getS().persist(user);
            asociarUserService(user);
            return true;
        }
        return false;
    }

    /**
     * Metodo que nos permite hacer login a usuarios indicandole su nombre de
     * usuario y contraseña, y así tener acceso a los servicios. Es tipo GET.
     *
     * @param username String de usuarios.
     * @param password String de contraseña.
     * @return Devuelve un JSON con un mensaje y el código de error.
     * @throws HibernateException
     * @throws Exception
     */
    public Token login(String username, String password) throws HibernateException, Exception {
        Token t = null;
        Query q = getS().createQuery(Queries.FIND_USER);
        q.setParameter("username", username);
        q.setParameter("password", encrypt(password));
        User u = (User) q.uniqueResult();
        if (u != null) {
            GregorianCalendar gc = new GregorianCalendar();
            //Porque el casteo de (Date) ??
            Date ultimaConexion = gc.getTime();
            String fecha = String.valueOf(gc.getTimeInMillis());
            gc.setTime(ultimaConexion);
            gc.add(Calendar.MINUTE, TIEMPO_TOKEN_ACTIVO);
            //Sobre la Query el porque Token u..??
            Query q1 = getS().createQuery(Queries.FIND_TOKEN_BY_USER_ID);
            q1.setParameter("idUser", u.getIdUser());
            List<Token> lista = q1.list();
            if (lista.isEmpty()) {
                t = new Token(u, encrypt(username + fecha), ultimaConexion,
                        gc.getTime(), new GregorianCalendar().getTime());
                //Ciuando tenemoq ue hacer un persist y cuando no..??
                getS().persist(t);
            } else {
                t = lista.get(0);
                actualizarFechasToken(lista.get(0));
            }
            asociarUserService(u);
        }
        return t;
    }

    /*
     * Actualiza las fechas de ultima conexion y la fecha de hasta cuando es
     * válido el token
     */
    public void actualizarFechasToken(Token token) {
        GregorianCalendar gc = new GregorianCalendar();
        token.setUltimaConexion(gc.getTime());
        gc.add(Calendar.MINUTE, TIEMPO_TOKEN_ACTIVO);
        token.setFechaExpira(gc.getTime());
        getS().persist(token);
    }

    /**
     * Método que devolverá true cuando se haya validado que el usuario existe,
     * está logeado y tiene privilegios para acceder al servicio
     *
     * @param name nombre del servicio que se va a ejecutar
     * @param token token del usuario que va a realizar la acción
     * @return true cuando el token está activo y tiene privilegios para acceder
     * al servicio
     * @throws HibernateException
     * @throws Exception
     */
    public boolean validar(String name, String token) throws HibernateException, Exception {
        Query qt = getS().createQuery(Queries.FIND_ALL_TOKENS_EXPIRED);
        qt.setParameter("currentTime", new GregorianCalendar().getTime());
        List<Token> listaTokens = qt.list();
        while (!listaTokens.isEmpty()) {
            getS().delete(listaTokens.get(0));
            listaTokens.remove(0);
        }
        Query q = getS().createQuery(Queries.FIND_TOKEN_BY_TOKEN);
        q.setParameter("token", token);
        Token tk = (Token) q.uniqueResult();
        //comprobamos que el token que nos han pasado existe
        if (tk != null) {
            //obtenemos el usuario asociado al token
            User u = tk.getUser();
            //y del usuario obtenido buscamos su id
            int idUser = u.getIdUser();
            Query q1 = getS().createQuery(Queries.FIND_SERVICE_BY_NAME);
            q1.setParameter("name", name);
            Service sv = (Service) q1.uniqueResult();
            //comprobamos que el servicio que quieren ejecutar está dado de alta
            if (sv != null) {
                int idService = sv.getIdService();
                //comprobamos que la hora de última conexión del token es mayor que la hora de última conexión más el tiempo estipulado para token activo
                Query q2 = getS().createQuery(Queries.CHECK_TOKEN_VALID);
                q2.setParameter("token", token);
                q2.setParameter("fechaActual", new GregorianCalendar().getTime());
                tk = (Token) q2.uniqueResult();
                //comprobamos que el token está aun activo
                if (tk != null) {
                    //por último comprobamos que el usuario tiene el nivel requerido para usar el servicio solicitado
                    Query q3 = getS().createQuery(Queries.CHECK_LEVEL_USER);
                    q3.setParameter("idUser", idUser);
                    q3.setParameter("idService", idService);
                    List<UserHasService> listado = q3.list();
                    actualizarFechasToken(tk);
                    //...??
                    return listado.size() > 0;
                }
            } else {
                throw new NullPointerException("No existe el servicio");
            }
        }
        return false;
    }

    private void asociarUserService(User usuario) {
        Query q = getS().createQuery(Queries.FIND_SERVICES);
        q.setParameter("level", usuario.getLevel());
        List<Service> services = q.list();
        if (!services.isEmpty()) {
            for (Service s : services) {
                q = getS().createQuery(Queries.FIND_USER_HAS_SERVICE);
                q.setParameter("user", usuario.getIdUser());
                q.setParameter("service", s.getIdService());
                if (!q.list().isEmpty()) {
                    continue;
                }
                UserHasService us = new UserHasService(s, usuario);
                getS().persist(us);
            }
        }
    }

}
