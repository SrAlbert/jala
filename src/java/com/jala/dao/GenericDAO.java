/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jala.dao;

import java.security.MessageDigest;
import java.util.Formatter;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author antonio
 */
public class GenericDAO {
    
    private Session s;
    private Transaction tx;

    public GenericDAO(Session s, Transaction tx) {
        this.s = s;
        this.tx = tx;
    }

    public GenericDAO(Session s) {
        this.s = s;
    }

    public Session getS() {
        return s;
    }

    public void setS(Session s) {
        this.s = s;
    }

    public Transaction getTx() {
        return tx;
    }

    public void setTx(Transaction tx) {
        this.tx = tx;
    }
    
    public String encrypt(String msg) {
        try {
            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(msg.getBytes("UTF-8"));
            return byteToHex(crypt.digest());
        } catch (Exception e) {
            return null;
        }
    }

    public String byteToHex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }
    
}
