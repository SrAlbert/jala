/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jala.util;

/**
 *
 * @author luks
 */
public class Piso {

    private int referencia;
    private String fechaAlta;
    private String tipo;
    private String operacióN;
    private String provincia;
    private int superficie;
    private int precioVenta;
    private String fechaVenta;
    private String vendedor;
    private String msg;

    public Piso(int referencia, String fechaAlta, String tipo, String operacion,
            String provincia, int superficie, int precioVenta, String fechaVenta,
            String vendedor) {
        this.referencia = referencia;
        this.fechaAlta = fechaAlta;
        this.tipo = tipo;
        this.operacióN = operacion;
        this.provincia = provincia;
        this.superficie = superficie;
        this.precioVenta = precioVenta;
        this.fechaVenta = fechaVenta;
        this.vendedor = vendedor;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}
