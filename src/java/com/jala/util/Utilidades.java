package com.jala.util;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import org.json.JSONObject;

public class Utilidades {
    static ArrayList<Piso> listaFinal = new ArrayList<>();
    public static ArrayList<Piso> buscaPorParametros(ArrayList<Parametro> parametros,
            JsonArray gsonArr) throws IOException {
        if (!parametros.isEmpty()) {
            ArrayList<Piso> listaJson = new ArrayList<>();
            for (JsonElement obj : gsonArr) {
                Piso piso;
                JsonObject gsonObj = obj.getAsJsonObject();
                int referencia1 = gsonObj.get("referencia").getAsInt();
                String fechaAlta1 = gsonObj.get("fechaAlta").getAsString();
                String tipo1 = gsonObj.get("tipo").getAsString();
                String operacion1 = gsonObj.get("operacióN").getAsString();
                String provincia1 = gsonObj.get("provincia").getAsString();
                int superficie1 = gsonObj.get("superficie").getAsInt();
                int precioVenta1 = gsonObj.get("precioVenta").getAsInt();
                String fechaVenta1;
                if (gsonObj.get("fechaVenta").isJsonNull()) {
                    fechaVenta1 = "null";
                } else {
                    fechaVenta1 = gsonObj.get("fechaVenta").getAsString();
                }
                String vendedor1;
                if (gsonObj.get("vendedor").isJsonNull()) {
                    vendedor1 = "null";
                } else {
                    vendedor1 = gsonObj.get("vendedor").getAsString();
                }
                if (parametros.get(0).getParam().equals("referencia")) {
                    if (String.valueOf(referencia1).equals(parametros.get(0).getValor())) {
                        //añadir los encontrados a la lista a devolver
                        piso = new Piso(referencia1, fechaAlta1, tipo1,
                                operacion1, provincia1, superficie1, precioVenta1, fechaVenta1, vendedor1);
                        listaJson.add(piso);
                    }
                }
                if (parametros.get(0).getParam().equals("fechaAlta")) {
                    //filtrar por fechaAlta
                    if (fechaAlta1.equals(parametros.get(0).getValor())) {
                        //añadir los encontrados a la lista a devolver
                        piso = new Piso(referencia1, fechaAlta1, tipo1,
                                operacion1, provincia1, superficie1, precioVenta1, fechaVenta1, vendedor1);
                        listaJson.add(piso);
                    }
                }
                if (parametros.get(0).getParam().equals("tipo")) {
                    //filtrar por tipo
                    if (tipo1.equals(parametros.get(0).getValor())) {
                        //añadir los encontrados a la lista a devolver
                        piso = new Piso(referencia1, fechaAlta1, tipo1,
                                operacion1, provincia1, superficie1, precioVenta1, fechaVenta1, vendedor1);
                        listaJson.add(piso);
                    }
                }
                if (parametros.get(0).getParam().equals("operacion")) {
                    //filtrar por operacióN
                    if (operacion1.equals(parametros.get(0).getValor())) {
                        //añadir los encontrados a la lista a devolver
                        piso = new Piso(referencia1, fechaAlta1, tipo1,
                                operacion1, provincia1, superficie1, precioVenta1, fechaVenta1, vendedor1);
                        listaJson.add(piso);
                    }
                }
                if (parametros.get(0).getParam().equals("provincia")) {
                    //filtrar por provincia
                    if (provincia1.equals(parametros.get(0).getValor())) {
                        //añadir los encontrados a la lista a devolver
                        piso = new Piso(referencia1, fechaAlta1, tipo1,
                                operacion1, provincia1, superficie1, precioVenta1, fechaVenta1, vendedor1);
                        listaJson.add(piso);
                    }
                }
                if (parametros.get(0).getParam().equals("superficie")) {
                    //filtrar por superficie
                    if (String.valueOf(superficie1).equals(parametros.get(0).getValor())) {
                        //añadir los encontrados a la lista a devolver
                        piso = new Piso(referencia1, fechaAlta1, tipo1,
                                operacion1, provincia1, superficie1, precioVenta1, fechaVenta1, vendedor1);
                        listaJson.add(piso);
                    }
                }
                if (parametros.get(0).getParam().equals("precioVenta")) {
                    //filtrar por precioVenta
                    if (String.valueOf(precioVenta1).equals(parametros.get(0).getValor())) {
                        //añadir los encontrados a la lista a devolver
                        piso = new Piso(referencia1, fechaAlta1, tipo1,
                                operacion1, provincia1, superficie1, precioVenta1, fechaVenta1, vendedor1);
                        listaJson.add(piso);
                    }
                }
                if (parametros.get(0).getParam().equals("vendedor")) {
                    //filtrar por precioVenta
                    if (vendedor1.equals(parametros.get(0).getValor())) {
                        //añadir los encontrados a la lista a devolver
                        piso = new Piso(referencia1, fechaAlta1, tipo1,
                                operacion1, provincia1, superficie1, precioVenta1, fechaVenta1, vendedor1);
                        listaJson.add(piso);
                    }
                }
            }
            parametros.remove(0);
            Gson gson = new Gson();
            gson.toJson(listaJson);
            JsonParser parser = new JsonParser();
            gsonArr = parser.parse(gson.toJson(listaJson)).getAsJsonArray();
            if (parametros.isEmpty()) {
                return listaJson;
            }
            listaFinal = buscaPorParametros(parametros, gsonArr);
        }
        return listaFinal;
    }
    
    public static String getHttpsURL(String url) throws IOException {
        URLConnection connection = new URL(url).openConnection();
        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1;"
                + " WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
        connection.connect();
        BufferedReader r = new BufferedReader(new InputStreamReader(connection.getInputStream(), Charset.forName("UTF-8")));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            sb.append(line);
        }
        return sb.toString();
    }

    public static Object[] getDatos(JSONObject jso){
        Object dato[]= new Object[9];
        
        dato[0]= jso.getString("name");//Nombre de la ciudad
        dato[1]= jso.getJSONArray("weather").getJSONObject(0).getString("description");//Tiempo que hace
        dato[2]= jso.getJSONObject("main").getInt("temp");//Temperatura actual en ºC
        dato[3]= jso.getJSONObject("main").getInt("temp_min");//Temperatura minima en ºC
        dato[4]= jso.getJSONObject("main").getInt("temp_max");//Temperatura maxima en ºC
        dato[5]= jso.getJSONObject("main").getInt("humidity");//Humedad en %
        dato[6]= jso.getJSONObject("wind").getInt("speed");//Viento en m/s
        dato[7]= jso.getJSONObject("sys").getLong("sunrise");//Hora a la que amanece
        dato[8]= jso.getJSONObject("sys").getLong("sunset");//Puesta de sol
        
        return dato;
    }
}