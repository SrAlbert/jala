/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jala.ws;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jala.util.HibernateUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author antonio
 */
public class GenericWS {

    private Session s;
    private Transaction tx;
    private Gson json;

    public GenericWS() {
        this.json = new Gson();
    }

    public void connect() throws HibernateException, Exception {
        s = HibernateUtil.getSessionFactory().getCurrentSession();
        tx = s.beginTransaction();
    }

    public String callRestWSByGet(String url) throws IOException {
        URL urlws = new URL(url);
        URLConnection uc = urlws.openConnection();
        uc.connect();
        //Creamos el objeto con el que vamos a leer
        BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
        String inputLine;
        String contenido = "";
        while ((inputLine = in.readLine()) != null) {
            contenido += inputLine + "\n";
        }
        in.close();
        return contenido;
    }

    public int llamaValidar(String token, String servicio) {
        String url;
        String jToken = "";
        if (token != null && !token.isEmpty()) {
            url = "http://127.0.0.1:8080/jala/rest/oauth/validar?"
                    + "token=" + token + "&nombre=" + servicio;
            try {
                jToken = callRestWSByGet(url);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            JsonParser parser1 = new JsonParser();
            JsonObject gsonArr1 = parser1.parse(jToken).getAsJsonObject();
            return gsonArr1.get("errorCode").getAsInt();
        }
        return -1;
    }

    /**
     * Este método se utiliza para desconectar cuando haya que hacer commit
     *
     * @param commit
     * @throws HibernateException
     * @throws Exception
     */
    public void disconnect(boolean commit) throws HibernateException, Exception {
        if (tx != null && tx.isActive()) {
            tx.commit();
        }
    }

    /**
     * Este método se utiliza para desconectar cuando no sea necesario hacer
     * commit
     *
     * @throws HibernateException
     * @throws Exception
     */
    public void disconnect() throws HibernateException, Exception {
        if (tx != null && tx.isActive()) {
            s.close();
        }
    }

    public Gson getJson() {
        return json;
    }

    public Session getS() {
        return s;
    }

    public Transaction getTx() {
        return tx;
    }

}
