package com.jala.ws;

import com.google.gson.JsonArray;
import org.json.JSONArray;
import org.json.JSONObject;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jala.bean.ResponseBean;
import com.jala.util.Parametro;
import com.jala.util.Piso;
import static com.jala.util.Urls.*;
import static com.jala.util.Utilidades.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.hibernate.HibernateException;

/**
 * REST Web Service
 *
 * @author Alberto
 */
@Path("service")
public class ServiceWS extends GenericWS {

    public ServiceWS() {
    }
    
    @Path("/servicio1_1")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String servicio1(@QueryParam("distrito_nombre") String distrito,
            @QueryParam("token") String token) throws IOException {
        ResponseBean rb = new ResponseBean();

        int errorCode = 0;
        if (distrito == null || distrito.isEmpty()
                || token == null || token.isEmpty()) {
            rb.setErrorCode(-100);
            rb.setMsg("Faltan parametros");
        } else {
            try {
                errorCode = llamaValidar(token, "servicio1");
                if (errorCode == 4) {
                    distrito = distrito.trim();

                    //Distritos con espacios dan error
                    if (distrito.contains(" ")) {
                        distrito = distrito.replaceAll(" ", "%20");
                    }

                    String url2 = ULR_SERVICIO1_1 + "distrito_nombre=" + distrito;

                    JSONObject jso = new JSONObject(callRestWSByGet(url2));
                    JSONArray jsa = jso.getJSONArray("@graph");
                    if (jsa.length() > 0) {
                        rb.setErrorCode(0);
                        rb.setMsg("OK");
                        rb.setObj(jsa);
                    }
                } else {
                    rb.setErrorCode(-500);
                    rb.setMsg("Token no válido.");
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                rb.setErrorCode(-600);
                rb.setMsg("Error generico");
            }
        }
        return getJson().toJson(rb);
    }

    @Path("/servicio1_2")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String servicio1(@QueryParam("latitud") String latitud,
            @QueryParam("longitud") String longitud,
            @QueryParam("distancia") String distancia,
            @QueryParam("token") String token) throws IOException {
        ResponseBean rb = new ResponseBean();

        int errorCode = 0;
        if (latitud == null || latitud.isEmpty()
                && longitud == null || longitud.isEmpty()
                && distancia == null || distancia.isEmpty()
                && token == null || token.isEmpty()) {
            rb.setErrorCode(-100);
            rb.setMsg("Faltan parametros");
        } else {
            try {
                errorCode = llamaValidar(token, "servicio2");
                if (errorCode == 4) {
                    latitud = latitud.trim();
                    longitud = longitud.trim();
                    distancia = distancia.trim();
                    String url2 = ULR_SERVICIO1_2 + "?latitud=" + latitud + "&longitud="
                            + longitud + "&distancia=" + distancia;

                    JSONObject jso = new JSONObject(callRestWSByGet(url2));
                    JSONArray jsa = jso.getJSONArray("@graph");
                    if (jsa.length() > 0) {
                        rb.setErrorCode(0);
                        rb.setMsg("OK");
                        rb.setObj(jsa);
                    } else {
                        rb.setErrorCode(-700);
                        rb.setMsg("No hay centros cerca");
                    }
                } else {
                    rb.setErrorCode(-500);
                    rb.setMsg("Token no válido.");
                }
            } catch (Exception e) {
                e.printStackTrace();
                rb.setErrorCode(-600);
                rb.setMsg("Error generico");
            }
        }
        return getJson().toJson(rb);
    }

    @Path("/servicio2")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String servicio2(@QueryParam("referencia") String referencia,
            @QueryParam("fechaAlta") String fechaAlta,
            @QueryParam("tipo") String tipo,
            @QueryParam("operacion") String operacion,
            @QueryParam("provincia") String provincia,
            @QueryParam("superficie") String superficie,
            @QueryParam("precioVenta") String precioVenta,
            @QueryParam("fechaVenta") String fechaVenta,
            @QueryParam("vendedor") String vendedor,
            @QueryParam("token") String token) {
        ResponseBean rb = new ResponseBean();
        int errorCode = 0;
        errorCode = llamaValidar(token, "servicio2");

        if (referencia != null && !referencia.isEmpty()
                || fechaAlta != null && !fechaAlta.isEmpty()
                || tipo != null && !tipo.isEmpty()
                || operacion != null && !operacion.isEmpty()
                || provincia != null && !provincia.isEmpty()
                || superficie != null && !superficie.isEmpty()
                || precioVenta != null && !precioVenta.isEmpty()
                || fechaVenta != null && !fechaVenta.isEmpty()
                || vendedor != null && !vendedor.isEmpty()) {
            if (errorCode == 4) {
                ArrayList<Piso> listaJson = new ArrayList<>();
                ArrayList<Parametro> parametros = new ArrayList<>();
                try {
                    if (referencia != null && !referencia.isEmpty()) {
                        Parametro p = new Parametro("referencia", referencia);
                        parametros.add(p);
                    }
                    if (fechaAlta != null && !fechaAlta.isEmpty()) {
                        Parametro p = new Parametro("fechaAlta", fechaAlta);
                        parametros.add(p);
                    }
                    if (tipo != null && !tipo.isEmpty()) {
                        Parametro p = new Parametro("tipo", tipo);
                        parametros.add(p);
                    }
                    if (operacion != null && !operacion.isEmpty()) {
                        Parametro p = new Parametro("operacion", operacion);
                        parametros.add(p);
                    }
                    if (provincia != null && !provincia.isEmpty()) {
                        Parametro p = new Parametro("provincia", provincia);
                        parametros.add(p);
                    }
                    if (superficie != null && !superficie.isEmpty()) {
                        Parametro p = new Parametro("superficie", superficie);
                        parametros.add(p);
                    }
                    if (precioVenta != null && !precioVenta.isEmpty()) {
                        Parametro p = new Parametro("precioVenta", precioVenta);
                        parametros.add(p);
                    }
                    if (fechaVenta != null && !fechaVenta.isEmpty()) {
                        Parametro p = new Parametro("fechaVenta", fechaVenta);
                        parametros.add(p);
                    }
                    if (vendedor != null && !vendedor.isEmpty()) {
                        Parametro p = new Parametro("vendedor", vendedor);
                        parametros.add(p);
                    }
                    String urlS = ULR_SERVICIO2;
                    String sJson = getHttpsURL(urlS);
                    JsonParser parser = new JsonParser();
                    JsonArray gsonArr = parser.parse(sJson).getAsJsonArray();
                    listaJson = buscaPorParametros(parametros, gsonArr);

                } catch (HibernateException e) {
                    rb.setMsg("Error de BBDD");
                    rb.setErrorCode(-400);
                } catch (Exception e) {
                    e.printStackTrace();
                    rb.setMsg("Error genérico");
                    rb.setErrorCode(-300);
                }
                return getJson().toJson(listaJson);
            } else {
                rb.setMsg("Token no válido");
                rb.setErrorCode(-500);
            }
        } else if (token != null && !token.isEmpty()) {
            String urlS = ULR_SERVICIO2;
            String sJson = "";
            try {
                sJson = getHttpsURL(urlS);
            } catch (IOException ex) {
                Logger.getLogger(ServiceWS.class.getName()).log(Level.SEVERE, null, ex);
            }
            JsonParser parser = new JsonParser();
            JsonArray gsonArr = parser.parse(sJson).getAsJsonArray();
            ArrayList<Piso> listaJson = new ArrayList<>();
            for (JsonElement obj : gsonArr) {
                Piso piso;
                JsonObject gsonObj = obj.getAsJsonObject();
                int referencia1 = gsonObj.get("referencia").getAsInt();
                String fechaAlta1 = gsonObj.get("fechaAlta").getAsString();
                String tipo1 = gsonObj.get("tipo").getAsString();
                String operacion1 = gsonObj.get("operacióN").getAsString();
                String provincia1 = gsonObj.get("provincia").getAsString();
                int superficie1 = gsonObj.get("superficie").getAsInt();
                int precioVenta1 = gsonObj.get("precioVenta").getAsInt();
                String fechaVenta1;
                if (gsonObj.get("fechaVenta").isJsonNull()) {
                    fechaVenta1 = "null";
                } else {
                    fechaVenta1 = gsonObj.get("fechaVenta").getAsString();
                }
                String vendedor1;
                if (gsonObj.get("vendedor").isJsonNull()) {
                    vendedor1 = "null";
                } else {
                    vendedor1 = gsonObj.get("vendedor").getAsString();
                }
                piso = new Piso(referencia1, fechaAlta1, tipo1,
                        operacion1, provincia1, superficie1, precioVenta1, fechaVenta1, vendedor1);
                listaJson.add(piso);
            }
            return getJson().toJson(listaJson);
        } else {
            rb.setMsg("Faltan parámetros!");
        }
        return getJson().toJson(rb);
    }

    @Path("/servicio3_1")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String servicio3_1(@QueryParam("ciudad") String ciudad,
            @QueryParam("pais") String pais,
            @QueryParam("token") String token) throws IOException {
        ResponseBean rb = new ResponseBean();
        
        if (ciudad == null || ciudad.isEmpty()
                && token == null || token.isEmpty()) {
            rb.setErrorCode(-100);
            rb.setMsg("Faltan parametros");
        } else {
            try {
                int errorCode= llamaValidar(token, "servicio3");
                if (errorCode == 4) {
                    ciudad = ciudad.trim();
                    
                    if(pais!=null && !pais.isEmpty()){
                        pais = pais.trim();
                        ciudad+= ","+pais;
                    }
                    
                    String url= ULR_SERVICIO3+"&q="+ciudad;

                    JSONObject jso = new JSONObject(callRestWSByGet(url));
                    Object dato[]= getDatos(jso);
                    if (dato.length > 0) {
                        rb.setErrorCode(0);
                        rb.setMsg("OK");
                        rb.setObj(dato);
                    } else {
                        rb.setErrorCode(-800);
                        rb.setMsg("No existe la ubicación");
                    }
                } else {
                    rb.setErrorCode(-500);
                    rb.setMsg("Token no válido.");
                }
            } catch (java.io.FileNotFoundException ex) {
                ex.printStackTrace();
                rb.setErrorCode(-900);
                rb.setMsg("No se encuentra la ciudad");
            } catch (Exception ex) {
                ex.printStackTrace();
                rb.setErrorCode(-600);
                rb.setMsg("Error generico");
            }
        }
        return getJson().toJson(rb);
    }
    
    @Path("/servicio3_2")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String servicio3_2(@QueryParam("latitud") String latitud,
            @QueryParam("longitud") String longitud,
            @QueryParam("token") String token) throws IOException {
        ResponseBean rb = new ResponseBean();
        
        if (latitud == null || latitud.isEmpty()
                && longitud == null || longitud.isEmpty()
                && token == null || token.isEmpty()) {
            rb.setErrorCode(-100);
            rb.setMsg("Faltan parametros");
        } else {
            try {
                int errorCode= llamaValidar(token, "servicio3");
                if (errorCode == 4) {
                    latitud = latitud.trim();
                    longitud = longitud.trim();
                    String url= ULR_SERVICIO3+"&lat="+latitud+"&lon="+longitud;

                    JSONObject jso = new JSONObject(callRestWSByGet(url));
                    Object dato[]= getDatos(jso);
                    if (dato.length > 0) {
                        rb.setErrorCode(0);
                        rb.setMsg("OK");
                        rb.setObj(dato);
                    } else {
                        rb.setErrorCode(-800);
                        rb.setMsg("No existe la ubicación");
                    }
                } else {
                    rb.setErrorCode(-500);
                    rb.setMsg("Token no válido.");
                }
            }catch (java.io.FileNotFoundException ex) {
                ex.printStackTrace();
                rb.setErrorCode(-900);
                rb.setMsg("No se encuentra la ubicación");
            } catch (Exception ex) {
                ex.printStackTrace();
                rb.setErrorCode(-600);
                rb.setMsg("Error generico");
            }
        }
        return getJson().toJson(rb);
    }
}
