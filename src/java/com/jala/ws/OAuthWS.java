package com.jala.ws;

import com.jala.bean.ResponseBean;
import com.jala.dao.UserDAO;
import com.jala.pojos.Token;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import org.hibernate.HibernateException;

/**
 * REST Web Service
 *
 * @author Alberto
 */
@Path("oauth")
public class OAuthWS extends GenericWS {

    @Context
    private UriInfo context;
    @Context
    private HttpServletRequest req;

    public OAuthWS() {
    }

    public OAuthWS(@Context HttpServletRequest req) {
        this.req = req;
    }

    /**
     * Metodo que nos permite dar de alta a nuevos usuarios indicandole su
     * nombre de usuario, contraseña y el nivel de acceso a los servicios. Es
     * tipo PUT. 
     *
     * @param username String de usuarios.
     * @param password String de contraseña.
     * @param level String del nivel.
     * @return Devuelve un JSON con un mensaje y el código de error.
     */
    @Path("/register")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public String register(@QueryParam("username") String username,
            @QueryParam("password") String password,
            @QueryParam("level") String level) {
        ResponseBean rb = new ResponseBean();
        try {
            if (username == null || username.trim().isEmpty()
                    || password == null || password.trim().isEmpty()
                    || level == null || level.isEmpty()) {
                rb.setErrorCode(-100);
                rb.setMsg("Fatan Parámetros");
            } else {
                username = username.trim();
                password = password.trim();
                level = level.trim();
                connect();
                UserDAO uDAO = new UserDAO(getS(), getTx());
                if (uDAO.register(username, password, level)) {
                    disconnect(true);
                    rb.setMsg("OK");
                    rb.setErrorCode(0);
                }else{
                    rb.setMsg("El usuario ya existe en el sistema.");
                    rb.setErrorCode(-800);
                }
            }
        } catch (HibernateException e) {
            e.printStackTrace();
            rb.setMsg("Error con la BBDD");
            rb.setErrorCode(-200);
        } catch (Exception e) {
            rb.setMsg("Error genérico");
            rb.setErrorCode(-300);
        }finally{
            try {
                disconnect(true);
            } catch (Exception e) {}
        }
        return getJson().toJson(rb);
    }

    public String getIp() throws ServletException, IOException {
        return req.getRemoteHost();
    }
    
    /**
     * Metodo que nos permite hacer login a usuarios indicandole su nombre de
     * usuario y contraseña, y así tener acceso a los servicios. Es tipo GET.
     *
     * @param username String de usuarios.
     * @param password String de contraseña.
     * @return Devuelve un JSON con un mensaje y el código de error.
     */
    @Path("/login")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String login(@QueryParam("username") String username,
            @QueryParam("password") String password) {
        ResponseBean rb = new ResponseBean();
        try {
            if (username == null || username.trim().isEmpty()
                    || password == null || password.trim().isEmpty()) {
                rb.setErrorCode(-100);
                rb.setMsg("Fatan Parámetros");
            } else {
                username = username.trim();
                password = password.trim();
                connect();
                    UserDAO uDAO = new UserDAO(getS(), getTx());
                Token token = uDAO.login(username, password);
                if (token != null) {
                    rb.setToken(token.getToken());
                    rb.setMsg("OK");
                    rb.setErrorCode(3);
                    // rb.getToken();
                } else {
                    rb.setMsg("Usuario no válido");
                    rb.setErrorCode(1);
                }
            }
        } catch (HibernateException e) {
            rb.setMsg("Error de BBDD");
            rb.setErrorCode(-400);
        } catch (Exception e) {
            e.printStackTrace();
            rb.setMsg("Error genérico");
            rb.setErrorCode(-300);
        }finally{
            try {
                disconnect(true);
            } catch (Exception e) {}
        }
        return getJson().toJson(rb);
    }

    /**
     * Nos permite validar si un token(usuario) tiene acceso a un determinado
     * servicio. Es tipo GET.
     *
     * @param token String que representa un token de usuario.
     * @param nombre String del nombre del servicio.
     * @return Devuelve un JSON con un mensaje y el código de error.
     */
    @Path("/validar")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String validar(@QueryParam("token") String token, @QueryParam("nombre") String nombre) {
        ResponseBean rb = new ResponseBean();
        try {
            if (getIp().equals("127.0.0.1")) {
                if (token == null || token.trim().isEmpty()
                        || nombre == null || nombre.trim().isEmpty()) {
                    rb.setErrorCode(-100);
                    rb.setMsg("Fatan Parámetros");
                } else {
                    token = token.trim();
                    nombre = nombre.trim();
                    connect();
                    UserDAO uDAO = new UserDAO(getS(), getTx());
                    if (uDAO.validar(nombre, token)) {
                        rb.setMsg("OK");
                        rb.setErrorCode(4);
                    } else {
                        rb.setMsg("Usuario no autorizado.");
                        rb.setErrorCode(2);
                    }
                }
            } else {
                rb.setMsg("Acceso denegado.");
                rb.setErrorCode(-600);
            }
        } catch (HibernateException e) {
            e.printStackTrace();
            rb.setMsg("Error de BBDD");
            rb.setErrorCode(-400);
        } catch (NullPointerException e) {
            e.printStackTrace();
            rb.setMsg(e.getMessage());
            rb.setErrorCode(-500);
        } catch (Exception e) {
            e.printStackTrace();
            rb.setMsg("Error genérico");
            rb.setErrorCode(-300);
        }finally{
            try {
                disconnect(true);
            } catch (Exception e) {}
        }
        return getJson().toJson(rb);
    }
}
