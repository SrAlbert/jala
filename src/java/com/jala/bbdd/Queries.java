/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jala.bbdd;

/**
 *
 * @author antonio
 */
public interface Queries {

    public static final String FIND_ALL_TOKENS_EXPIRED = "from Token where fechaExpira < :currentTime";

    public static final String FIND_USER = "from User where username = :username "
            + "and password = :password";
    
    public static final String CHECK_USER = "from User where username = :username";

    public static final String CHECK_TOKEN_VALID = "from Token where "
            + "token = :token and "
            + "fechaExpira >= :fechaActual";

    public static String CHECK_LEVEL_USER = "from UserHasService u where "
            + "u.user.idUser = :idUser and u.service.idService = :idService";

    public static String FIND_USER_BY_NAME = "from User where username = :username";

    public static String FIND_TOKEN_BY_TOKEN = "from Token where token = :token";

    public static String FIND_TOKEN_BY_USER_ID = "from Token u where u.user.idUser = :idUser";

    public static String FIND_SERVICE_BY_NAME = "from Service where name = :name";
    
    public static String FIND_SERVICES = "from Service where level <= :level";

    public static String FIND_USER_HAS_SERVICE = "from UserHasService where user_idUser= :user and service_idService= :service";
    
}
